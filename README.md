## Windows
- Download PHP7 and extract it to C:\PHP
- Add C:\PHP to PATH
- Download PEAR from https://pear.php.net/manual/en/installation.getting.php and save go-pear.phar to C:\PHP
- Start an elevated cmd in C:\php and run `php go-pear.phar`
- Import the generated .reg file into your windows registry (Should be in  C:\PHP)
- Run the following from within the elevated cmd promp: ```pear install PHP_CodeSniffer```
- Checkout https://gitlab.com/brightfish/php/codestyle.git to C:\BFPHPCodeStyle
- Run ```phpcs --config-set installed_paths C:\BFPHPCodeStyle```
- Run ```phpcs --config-set default_standard BF```

## OSX

```bash
###
# Note: Ensure composer is installed globally
###

# Install latest PHPCS globally via composer
composer global require "squizlabs/php_codesniffer=*"

# Remove existing install of phpcs and phpcbf to path
sudo rm -rf /usr/local/bin/{phpcs,phpcbf} 2> /dev/null

# Link phpcs and phpcbf to bin
ln -s ~/.composer/vendor/bin/phpcs /usr/local/bin/phpcs
ln -s ~/.composer/vendor/bin/phpcbf /usr/local/bin/phpcbf

# Delete the existing dir if exists
sudo rm -rf ~/BFPHPCodeStyle/ 2> /dev/null

# Clone the BF coding standard into correct location
git clone https://gitlab.com/brightfish/php/codestyle.git ~/BFPHPCodeStyle/

# Add the standard to the sniffers path and set it as the default standard
sudo phpcs --config-set installed_paths ~/BFPHPCodeStyle/BF
sudo phpcs --config-set default_standard BF
```

## Install Sublime packages

- EditorConfig
- SublimeLinter
- SublimeLinter-php
- SublimeLinter-phpcs

Restart SublimeText.

### Sublime config

#### User
````
{
    "trim_trailing_white_space_on_save": true,
    "ensure_newline_at_eof_on_save": true,
    "trim_automatic_white_space": true,
    "translate_tabs_to_spaces": true,
    "default_line_ending": "unix",
    "tab_size": 4
}
````

#### Package - SublimeLinter - User
````
{
    "linters": {
        "php": {
            "@disable": false,
            "args": [],
            "excludes": []
        },
        "phpcs": {
            "@disable": false,
            "args": [],
            "excludes": [],
            "standard": "BF"
        }
    }
}
````

Note: The first time you save the "SublimeLinter - User" preferences and restart sublime it will reset the config file
